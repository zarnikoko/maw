$(document).ready(function(){
	comSelect2();
});
function addRow(){
	var html = '';
	html += '<tr>';
	html += '<td><select class="country" name="country[]" style="width:235px;">';
	html += '<option></option>';
	html += '<option value="usa">USD(US Dollar)</option>';
    html += '<option value="euro">Euro(EUR) </option>';
    html += '<option value="malaysian">Malaysian Ringgit(MYR)</option>';
    html += '<option value="singapore">Singapore Dollar(SGD)</option>';
    html += '<option value="thai">Thai Baht (THB)</option>';
	html += '</select></td>';
	html += '<td><input type="text" name="buy[]" size="15"></td>';
	html += '<td><input type="text" name="sell[]" size="15"></td>';
	html += '<td><span onclick="addRow()" class="sppointer">+</span></td>';
	html += '<td><span onclick="removeRow(this)" class="sppointer">-</span></td>';
	html += '</tr>';

  if($("#tb tr").length!=5){
	$("#tb").append(html);
	comSelect2();
  }
  else{
  	 alert("Can't add anymore");
  }
}

function removeRow(row){
	console.log(row);
	if($("#tb tr").length!=1){
	row.closest("tr").remove();
    }
    else{
      alert("Can't delete anymore");
    }
}

function comSelect2(){
	$('.country').select2({
		templateResult: formatState
	});
}

function formatState (state) {
	if (!state.id) {
		return state.text;
	}
	var baseUrl = "../images";
	var $state = $(
		'<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" width="25px" height="20px" class="img-flag" /> ' + state.text + '</span>'
	);
	return $state;
};
