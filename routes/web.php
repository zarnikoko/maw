<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['middleware'=>['web']],function(){

Route::get('/','UserController@index');
Route::get('/user/login','UserController@index');
Route::post('/user/login','UserController@login');
Route::get('/user/register','UserController@register');
Route::post('/user/register','UserController@create_user');
Route::get('/user/home','UserController@home');
Route::get('/user/logout','UserController@logout');
Route::post('/user/ASL','UserController@BB_ASL');
Route::post('/user/BB_ASL','UserController@add_sales_list');
Route::post('/user/PS','UserController@BB_PS');
Route::post('/user/BB_PS','UserController@BB_PS_Paid');
Route::post('/user/Update_PS','UserController@Update_PS');
Route::post('/user/DSL','UserController@BB_DSL');
Route::post('/user/BB_DSL','UserController@BB_DSL_Del');
Route::post('/user/VSL','UserController@BB_VSL');

});
