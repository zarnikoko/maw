
<!DOCTYPE html>
<html lang="en">
<head>
</head>
@include("user/common")
  <div class="row justify-content-center mt-1">
    <div class="col-3" style="height:580px;border:2px solid blue">
      <div class="row pb-2 pt-2">
         <div class="col border bg-primary p poi btn w" id="BB">
              <img src="../images/BB.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label" >
                  <span>အလှူမင်္ဂလာ </span>
                  <span style="border-bottom:2px solid lightgrey"><br>အလှမိတ်ကပ်</span>
                  <span class="y"> <br> BB </span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="BB_Form">
                  {{ csrf_field()  }}
                  <input type="hidden" value="BB" name="category"> 
              </form>
        </div>
        <div class="col border bg-primary p poi btn w" id="BD">
              <img src="../images/BD.jpg" width="90" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span style="border-bottom:2px solid lightgrey"><br>ဘွဲ့မိတ်ကပ်</span>
                  <span class="y"> <br> BD </span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="BD_Form">
                 {{ csrf_field()  }}
                  <input type="hidden" value="BD" name="category"> 
              </form>
        </div>
        <div class="col border bg-primary p poi btn w"  id="MJ">
              <img src="../images/j.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span style="border-bottom:2px solid lightgrey"><br>ရတနာ</span>
                  <span class="y"> <br> MJ </span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="MJ_Form">
                 {{ csrf_field()  }}
                  <input type="hidden" value="MJ" name="category"> 
              </form>
        </div>
      </div>
      <div class="row pb-2">
        <div class="col border bg-primary p poi btn w" id="DB">
              <img src="../images/DB.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span>အလှူမင်္ဂလာ<br></span>
                  <span style="border-bottom:2px solid lightgrey">ဝတ်စုံ </span>
                  <span class="y"> <br> DB </span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="DB_Form">
                {{ csrf_field()  }}
                  <input type="hidden" value="DB" name="category"> 
              </form>
        </div>
        <div class="col border bg-primary p poi btn w" id="DD">
              <img src="../images/DD.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span>ဘွဲ့ဝတ်စုံ</span>
                  <span style="border-bottom:2px solid lightgrey">ပြည်တွင်း </span>
                  <span class="y"> <br> DD </span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="DD_Form">
                {{ csrf_field()  }}
                  <input type="hidden" value="DD" name="category"> 
              </form>
        </div>
        <div class="col border bg-primary p poi btn w"  id="DF">
              <img src="../images/fconvo.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span>နိုင်ငံခြားဘွဲ့</span>
                  <span style="border-bottom:2px solid lightgrey">ဝတ်စုံ </span>
                  <span class="y"> <br> DF </span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="DF_Form">
                {{ csrf_field()  }}
                  <input type="hidden" value="DF" name="category"> 
              </form>
        </div>
      </div>
      <div class="row pb-2">
        <div class="col border bg-primary p poi btn w" id="MC">
              <img src="../images/MC.png" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span>&nbsp;ကား <br></span>
                  <span style="border-bottom:2px solid lightgrey">လက်မှတ် </span>
                  <span class="y"> <br>MC</span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="MC_Form">
               {{ csrf_field()  }}
                  <input type="hidden" value="MC" name="category"> 
              </form>
        </div>
      <div class="col border bg-primary p poi btn w" id="MF">
              <img src="../images/f.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span>ပုံ၊ဘောင်၊</span>
                  <span style="border-bottom:2px solid lightgrey">လိုင်စင် </span>
                  <span class="y"> <br>MF</span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="MF_Form">
                {{ csrf_field()  }}
                  <input type="hidden" value="MF" name="category"> 
              </form>
      </div>
      <div class="col border bg-primary p poi btn w" id="MS">
              <img src="../images/beauty2.jpg" width="70" height="90" class="rounded-circle">
              <label class="cathe_label">
                  <span>Beauty<br></span>
                  <span style="border-bottom:2px solid lightgrey">&nbsp;Salon </span>
                  <span class="y"> <br>MS</span>
              </label>
              <form method="post" action="{{URL::to('user/DSL')}}" id="MS_Form">
                {{ csrf_field()  }}
                  <input type="hidden" value="MS" name="category"> 
              </form>
      </div>
     </div>
    </div>
    <div class="col-9" style="height:580px;border:2px solid blue;border-left:none;overflow-y:auto;" id="mydiv">
       <ul class="nav nav-tabs nav-justified sticky-top">
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_ASL">
                အရောင်းစာရင်းအပ်ရန်
                <form method="post" action="{{URL::to('user/ASL')}}" id="a_ASL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden" value="{{$category}}" name="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_PS">
                ငွေပေး၊ငွေချေရန်
                <form method="post" action="{{URL::to('user/PS')}}" id="a_PS_Form">
                    {{ csrf_field()  }}
                    <input type="hidden" value="{{$category}}" name="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" style="color:red" id="a_DSL">
                စာရင်းဖျက်ရန်
                <form method="post" action="{{URL::to('user/DSL')}}" id="a_DSL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden" value="{{$category}}" name="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_VSL">
                စာရင်းချုပ်ကြည့်ရန်
                <form method="post" action="{{URL::to('user/VSL')}}" id="a_VSL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden" value="{{$category}}" name="category">
                </form>
              </a>
                </li>
            </ul>
        <div style="background-color:white;" class="pt-3 pb-5" id="list_main">
          <div class="row pl-2">
            <div class="col-md-4 position-fixed" style="z-index:1">
              <input type="text" class="form-control" placeholder="Search..." id="serach_list"
              style="width:12em;">
            </div>
            <div class="col-md-5 offset-md-4 position-fixed" style="z-index:1;">
              <label style="float:right;" class="text-info mr-5"> 
                ဘောင်ချာစုစုပေါင်း
                  <span class="bg-warning btn rounded-circle" style="cursor:default;"> 
                      @if(isset($saleList))
                          {{ sizeof($saleList) }}
                      @endif
                      @if(!isset($saleList))
                           {{0}}
                      @endif
                  </span>
              </label>
            </div>
          </div>
          @if(isset($saleList))
              @foreach($saleList as $sList)
          <div class="row justify-content-center" id="list_container">
            <div class="col-md-auto bg-secondary list rounded mt-5 mb-1" 
            style="color:white;cursor:pointer" id="{{ $sList->id }}">
                 <table>
                    <thead>
                  <!-- <tr style="border-top:2px solid #dfdfdf"> -->
                    <tr style="border-top:1px solid #dfdfdf">
                              <th style="padding:10px 50px 0 40px;">{{$sList->name}}</th>
                      <th style="padding:10px 50px 0 40px;">{{$sList->receipt_no}}</th>
                  </tr>
                  <tr style="border-bottom:1px solid #dfdfdf">
                              <th style="padding:0 40px 0 40px;">{{$sList->phone}}</th>
                      <th style="padding:10px 40px 10px 40px;">{{$sList->date}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                              <td style="padding:30px 40px 10px 40px;">သင့်ငွေ</td>
                      <td style="padding:30px 40px 10px 40px;">{{$sList->c_money}}</td>
                  </tr>
                  <tr>
                              <td style="padding:10px 40px 10px 40px;">စရံ</td>
                      <td style="padding:10px 40px 10px 40px;">{{$sList->a_payment}}</td>
                  </tr>

                  <tr>
                              <td style="padding:10px 40px 10px 40px;">ချေပြီးငွေ</td>
                      <td style="padding:10px 40px 10px 40px;">{{$sList->pay_amt}}</td>
                  </tr>
                  <tr>
                              <td style="padding:10px 40px 10px 40px;">ကျန်ငွေ</td>
                      <td style="padding:10px 40px 10px 40px;">{{$sList->r_balance}}</td>
                  </tr>
                  <tr>
                              <td style="padding:10px 40px 80px 40px;">ငွေချေသောနေ့</td>
                      <td style="padding:10px 40px 80px 40px;">{{$sList->pay_date}}</td>
                  </tr>
                </tbody>
                  </table>    
            </div>
          </div>
              @endforeach
              @endif
        </div>
        <div style="background-color:white" class="pt-2">
        <input type="button" value="Back" class="btn btn-secondary" id="back" style="display:none">
       <!--  <input type="button" value="Top" class="btn btn-secondary" id="top" style="display:none"> -->
        </div>
        <div style="background-color:white;display:none" class="row justify-content-center" id="pay_form">
          <form method="post" action="{{ URL::to('user/BB_DSL') }}" id="del_form">
            <div class="bg-secondary col-md-auto pt-2 mt-4 mb-5 rounded" style="color:white">
              <table>
                    <thead>
                  <!-- <tr style="border-top:1px solid #dfdfdf;"> -->
                  <tr>
                              <th style="padding:0 40px 0 40px;"><label id="cname"></label></th>
                      <th style="padding:0 40px 0 40px;"><label id="receipt"></label></th>
                  </tr>
                  <tr style="border-bottom:1px solid #dfdfdf">
                              <th style="padding:0 40px 0 40px;"><label id="phone"></label></th>
                      <th style="padding:10px 40px 10px 40px;"><label id="date"></label></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                              <td style="padding:10px 40px 10px 40px;">သင့်ငွေ</td>
                      <td style="padding:10px 40px 10px 40px;">
                        <input type="number" class="form-control col-12" id="cmoney" readonly>
                      </td> 
                  </tr>
                  <tr>
                              <td style="padding:10px 40px 10px 40px;">စရံ</td>
                      <td style="padding:10px 40px 10px 40px;">
                        <input type="number" class="form-control col-12" id="apay" readonly>
                      </td>
                  </tr>

                  <tr>
                              <td style="padding:10px 40px 10px 40px;">ချေပြီးငွေ</td>
                      <td style="padding:10px 40px 10px 40px;">
                        <input type="number" class="form-control col-12" id="pamt" name="pamt" readonly>
                      </td>
                  </tr>
                  <tr>
                              <td style="padding:10px 40px 10px 40px;">ကျန်ငွေ</td>
                      <td style="padding:10px 40px 10px 40px;">
                        <input type="number" class="form-control col-12" id="ramt" name="ramt" readonly>
                      </td>
                  </tr>
                </tbody>
                  </table>
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="category" name="category" value="{{$category}}">
                    {{ csrf_field()  }}
                     <center> 
                      <input type="button" value="ဖျက်ရန်" class=" btn btn-danger mb-4 mt-4 pl-4 pr-4" id="del"> 
                     </center>
              </div> 
          </form>   
          <input type="hidden" id="scroll_get">
        </div>
    </div>
    </div>
 </div>
 <div id="divpopup" style="display:none;color:white;z-index:999999" class="bg-danger">
ဖျက်ရန်သေချာပါသလား?
 </div>
</body>
<script>
  $(document).ready(function(){
        $(".sticky-top").css("z-index",100)
        // $(".w label").css("background-color","rgba(0,0,0,0.1)");

        // $(".w").mouseenter(function(){
        //     $(this).css("color","white");
        //     $('label',this).css("background-color","rgba(0,0,0,0.2)");
        // }).mouseleave(function(){
        //     $('label',this).css("background-color","rgba(0,0,0,0.1)");

        // });
       <?if(!isset($saleList)){ ?>
             $("#list_main").css("height","480px");
       <? } ?>
     $("#serach_list").on("keyup", function() {
        var value = $(this).val().toLowerCase();
           $("#list_container .list").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
    });

    $("#serach_list").on("change", function() {
        var value = $(this).val().toLowerCase();
           $("#list_container .list").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
     });
    //   <?if(isset($saleList)){ ?>
    //       var sList = <?= json_encode($saleList) ?>;
  
    //       var len = sList.length;
    //       if(len>1){
    //           $("#top").show();
    //       }
    //  <? } ?>
    // $("#top").click(function(){
    //    $("#mydiv").scrollTop(0);
    // });

     $(".list").click(function(){
      var cur_scroll  = $("#mydiv").scrollTop();
      $("#scroll_get").val(cur_scroll);
      $("#list_main").hide();
      $("#back").show();
      $("#top").css("display","none");
      $("#pay_form").show();
      $("#mydiv").scrollTop(0);
        var id=this.id;
       <?if(isset($saleList)){ ?>
              var sList = <?= json_encode($saleList) ?>;
             <? } ?>
              var len = sList.length;
              for(var i=0;i<len;i++){
                if(sList[i].id==id){
                  var id      = sList[i].id;
                  var name    = sList[i].name;
                  var receipt = sList[i].receipt_no;
                  var phone   = sList[i].phone;
                  var date    = sList[i].date;
                  var cmoney  = sList[i].c_money;
                  var apay    = sList[i].a_payment;
                  var pamt    = sList[i].pay_amt;
                  var ramt  = sList[i].r_balance;

                  $("#id").val(id);
                  $("#cname").html(name);
                  $("#receipt").html(receipt);
                  $("#phone").html(phone);
                  $("#date").html(date);
                  $("#cmoney").val(cmoney);
                  $("#apay").val(apay);
                  $("#pamt").val(pamt);
                  $("#ramt").val(ramt);
                }
              }
     });
     // $("#pay_form").hide();
     $("#del").click(function(){
         // var r = confirm("Are you sure to delete this receipt?");
         // if(r==true){
         //      $("#del_form").submit();    
         // }
         // else{
            
         // }
         $("#divpopup").dialog({
              title :"ဖျက်ရန်အတည်ပြုချက်",
              width :430,
              height:200,
              modal :true,
              buttons:{
                ဖျက်မည်:
                  function(){
                    $("#del_form").submit();
                  },
                မဖျက်ပါ:
                  function(){
                    $(this).dialog('close');
                  }
              }
         });

         $(".ui-dialog .ui-dialog-titlebar-close").css("display","none"); 
     });
     
       $("#{{$category}}").removeClass("bg-primary");
        $("#{{$category}}").addClass("bg-success");

        $("#BB").click(function(){
            $("#BB_Form").submit();
        });

        $("#BD").click(function(){
            $("#BD_Form").submit();
        });

        $("#MJ").click(function(){
            $("#MJ_Form").submit();
        });

        $("#DB").click(function(){
            $("#DB_Form").submit();
        });

        $("#DD").click(function(){
            $("#DD_Form").submit();
        });

        $("#DF").click(function(){
            $("#DF_Form").submit();
        });

        $("#MC").click(function(){
            $("#MC_Form").submit();
        });

        $("#MF").click(function(){
            $("#MF_Form").submit();
        });
         
        $("#MS").click(function(){
            $("#MS_Form").submit();
        });

        $("#a_ASL").click(function(){
          $("#a_ASL_Form").submit();
        });

        $("#a_PS").click(function(){
          $("#a_PS_Form").submit();
        });

        $("#a_DSL").click(function(){
          $("#a_DSL_Form").submit();
        });

        $("#a_VSL").click(function(){
          $("#a_VSL_Form").submit();
        });

        $("#back").click(function(){
            var scroll_value=$("#scroll_get").val();
            $("#pay_form").hide();
            $("#list_main").show();
            $("#back").hide();
            $("#mydiv").scrollTop(scroll_value);

           // <?if(isset($saleList)){ ?>
           //    var sList = <?= json_encode($saleList) ?>;
           
           //    var len = sList.length;
           //    if(len>1){
           //      $("#top").show();
           //    }
           //  <? } ?>

        });

        <? if (isset($DSL_Success)) : ?>
        alert("ဒေတာဖျက်ခြင်းအောင်မြင်ပါသည်။");
        <? endif; ?>
  });

</script>
</html>