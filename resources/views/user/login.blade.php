<!DOCTYPE html>
<html lang="en">
<head>
</head>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<style>
.container-fluid{
  height:100vh;
}
</style>
<meta charset="UTF-8">
<title>Maw</title>
<body class="bg-light">
  <!-- <h1> Banking System </h1> -->
@if(count($errors)>0)
<div class="alert alert-danger errors fixed-top">
  <ul>
    @foreach ($errors->all() as $err)
      <li> {{ $err }} </li>
    @endforeach
  </ul>
</div>
@endif

@if(session('info'))
<div class="alert alert-success info fixed-top">
  {{session('info')}}
</div>
@endif
<div class="container-fluid">
   <div class="d-flex flex-column justify-content-center h-100 align-items-center">
        <div class="flex-column justify-content-center border shadow rounded" style="background:white">
            <div class="bg-primary pt-1 pb-1 shadow" id="user_login">
                <center>
                  <label style="font-size:25px;font-weight:400;color:white">User Login</label>
                </center>
             </div>
        <div class="d-flex justify-content-center pr-5 pb-5 pt-5 pl-3" id="login_form">
            <div style="width:180px;height:180px;" class="mr-3 mt-3">
                <img src="../images/mawlogo_login.png" width="100%" height="100%">
            </div>
            <form method="post" action="{{ URL::to('user/login') }}">
               {{ csrf_field()  }}
              <div class="form-group">
                <label for="username">UserName:</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Enter UserName">
              </div>
              <div class="form-group mb-5">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" name="password" 
                placeholder="Enter Password">
              </div>
              <!-- <div class="form-group form-check" style="visibility:hidden;">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox"> Remember me
                </label>
            </div> -->
              <button type="submit" class="btn btn-primary">Login</button>
              <a href="{{ URL::to ('user/register') }}" style="color:green"> 
              Register New Account 
                        </a>
            </form>
        </div>
      </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){

    $(".errors").show();
    $(".info").show();

    $("#username").click(function(){
      $(".errors").hide();
      $(".info").hide();
    });

    $("#password").click(function(){
      $(".errors").hide();
      $(".info").hide();
    });
	});
</script>