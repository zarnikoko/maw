<?php 
namespace App\Models;
use DB;
use Hash;
use Carbon\Carbon;


class users {

  static function insert($input){
    
    $result = DB::table('users')->insertGetId([
      'username'       => $input['username'],
      'password'       => Hash::make($input['password'])
    ]);
    return $result;
 	}

  static function insert_sales($input){
  
    $created_at = Carbon::now();
    $created_at->toDateTimeString();
    $result = DB::table('sales')->insert([
      'receipt_no'       => $input['receipt_number'],
      'name'             => $input['name'],
      'address'          => $input['address'],
      'phone'            => $input['phno'],
      'date'             => $input['date'],
      'c_money'          => $input['cmoney'],
      'a_payment'        => $input['a_pay'],
      'r_balance'        => $input['r_b'],
      'type'             => $input['category'],
      'created_at'       => $created_at,
      'updated_at'       => $created_at
    ]);

    return $result;
  }

  static function paid_sales($input){
    $updated_at = Carbon::now();
    $updated_at->toDateTimeString();

    $result = DB::table('sales')->where('id',$input['id'])->update([
      'pay_amt'         => $input['clone_pamt'],
      'r_balance'       => $input['ramt'],
      'pay_date'        => $input['pay_date'],
      'updated_at'      => $updated_at

    ]);
    return $result;
  }

  static function update_sales($input){
    $updated_at = Carbon::now();
    $updated_at->toDateTimeString();

    $result = DB::table('sales')->where('id',$input['eid'])->update([
      'pay_amt'         => $input['epaid_amt'],
      'r_balance'       => $input['eramt'],
      'pay_date'        => $input['epay_date'],
      'updated_at'      => $updated_at
    ]);
    return $result;
  }

  static function del_sales($id){
    $result = DB::table('sales')->where('id',$id)->delete();
    return $result;
  }

//   static function update($input){
//     $modified_at = Carbon::now();
//     $modified_at-> toDateTimeString(); 

//     if($input['password']){
//       $result = DB::table('users')->where('id',$input['accno'])->update([
//         'name'        => $input['name'],
//         'amount'      => $input['amount'],
//         'phone'       => $input['phone'],
//         'address'     => $input['address'],
//         'password'    => Hash::make($input['password']),
//         'modified_at' => $modified_at,
//         'lock'        => $input['status'],
//         'role'        => $input['type']
//       ]);
//     }else{
//       $result = DB::table('users')->where('id',$input['accno'])->update([
//         'name'        => $input['name'],
//         'amount'      => $input['amount'],
//         'phone'       => $input['phone'],
//         'address'     => $input['address'],
//         'modified_at' => $modified_at,
//         'lock'        => $input['status'],
//         'role'        => $input['type']
//       ]);
//     }
//     return $result;
//   }

//   static function delete($user_id){ 
//     $result = DB::table('users')->where('id',$user_id)->delete();
//     return $result;
//   }

//   static function update_amount($input){
//     $modified_at = Carbon::now();
//     $modified_at-> toDateTimeString();

//     $result = DB::table('users')->where('id',$input[1])->update([
//       'amount'      => $input[0],
//       'modified_at' => $modified_at
//     ]);
//    return $result;
//   }

//   static function update_psw($input){
//     $modified_at = Carbon::now();
//     $modified_at-> toDateTimeString();

//     $result = DB::table('users')->where('id',$input[1])->update([
//       'password'    => Hash::make($input[0]),
//       'modified_at' => $modified_at
//     ]);
//     return $result;
//   }

//   static function wc_reset($input){

//     $result = DB::table('users')->where('id',$input)->update([
//         'wrong_count' => 0
//      ]);

//     return $result;
//   }

//    static function is($user,$role){

//            if($user->role==$role){
//             return true;
//            }
//            else{
//             return false;
//            }
//     }
}

?>