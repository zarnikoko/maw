$(document).ready(function(){
	$("#contact").css("cursor","pointer");
	$(".ui-widget-overlay").css({"background":"black","opacity":"0.7"});
	$("#contact").click(function(){
		 $("#contactpopup").dialog({
              title :"Contact us",
              width :576,
              height:379,
              modal :true,
              buttons:{
                OK:
                  function(){
                    $(this).dialog('close');
                  }
              }
         });
         $(".ui-dialog-buttonset").css("float","none");
         $(".ui-dialog-buttonset").addClass("text-center");
		 $(".ui-dialog-buttonset").children().addClass("btn btn-primary pl-4 pr-4");
         $(".ui-dialog .ui-dialog-titlebar-close").css("display","none"); 
	});
});