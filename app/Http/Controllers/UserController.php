<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use Request;
use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use App\Models\users; 
use App\User;
use App\Notifications\BalanceNoti;
use App\Notifications\BalanceHistory;
use Illuminate\Notifications\Notifiable;


class UserController extends Controller{

use Notifiable;
	function index() {
    if(Auth::check())
    return view("user/home");

    return view("user/login");
	}

  function register(){
    return view("user/register");
  }

  function create_user(){
    $input    = Request::all();
    $validator = Validator::make($input,array(
        'username'       => 'required|unique:users,username',
        'password'       => 'required',
        'passwordAgain' => 'same:password'
    ));
    if($validator->passes()){
      $id = users::insert($input);
      Auth::loginUsingId($id);
      Auth::logout();
      return redirect('user/register')->with('info','Successful Created A User Account');
    }
    else{
        
        return redirect('user/register')->withErrors($validator);
    }

  }

  function login(){
    $input = Request::all();
    if(Auth::attempt([
      'username' => $input['username'], 
      'password' => $input['password']
    ]
    ))
     {
        return redirect('/user/home');
     }
    else{
        return redirect('/user/login')->withErrors(array('Failed Login! Try Again'));
  }
    
    }

    function home(){
      if(Auth::check())
      return view('user/home');

      return redirect('user/login');
    }

    function logout(){
      Auth::logout();
      return redirect('user/login')->with("info","Successful Logout");
    }

    function BB_ASL(){
      if(Auth::check()){
      $input    = Request::all();
      $category = $input['category'];
      return view('user/BB_ASL',array(
        'category'  => $category
      ));
    }

      return view('user/login');
    }

    function add_sales_list(){
      $input     = Request::all();
      $category  = $input['category'];
      $validator = Validator::make($input,array(
        'receipt_no'     => 'required|digits:6',
        'receipt_number' => 'unique:sales,receipt_no',
        'name'           => 'required',
        'address'        => 'required',
        'phno'           => 'required|numeric',
        'date'           => 'required',
        'cmoney'         => 'required|numeric',
        'a_pay'          => 'required|numeric',
        'r_b'            => 'required|numeric'
      ));
      if($validator->passes()){

        users::insert_sales($input);
        return view('user/BB_ASL',array(
          'category' => $category,
          'ASL_Success' => "Adding Data Successful"
        ));

      }
      else{
        
          return view('user/BB_ASL',array(
            'category' => $category,
            'input'    => $input
          ))->withErrors($validator);
      }

    }

    function BB_PS(){
      if(Auth::check()){
      $input     = Request::all();
      $category  = $input['category'];
      $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
      $test=json_decode($sale_list,true);

      if(!empty($test)){
         return view('user/BB_PS',array(
        'saleList' => $sale_list,
        'category' => $category
      ));

      }

      else{
      return view('user/BB_PS',array(
        'category' => $category
      ));
      }

      } 

      else{

      return view('user/login');

      }
     
    }
    function BB_PS_Paid(){
      $input     = Request::all();
      $category  = $input['category'];
      $validator = Validator::make($input,array(
        'pamt'   => "required|numeric"
      ));

      if($validator->passes()){
        users::paid_sales($input);
        $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
        return view('user/BB_PS',array(
              'category' => $category,
              'saleList'=> $sale_list,
              'PS_Success'=> "Paid Sale Successful"
        ));
      }
      else{
        $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
        return view('user/BB_PS',array(
              'category' => $category,
              'saleList'=> $sale_list
        ))->withErrors($validator);

      }

    }

  function Update_PS(){
      $input     = Request::all();
      $category  = $input['ecategory'];
      $validator = Validator::make($input,array(
        'epaid_amt'   => "numeric"
      ));

      if($validator->passes()){
        users::update_sales($input);
        $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
        return view('user/BB_PS',array(
              'category' => $category,
              'saleList'=> $sale_list,
              'update_success'=> "Update Sale Successful"
        ));
      }
      else{
        $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
        return view('user/BB_PS',array(
              'category' => $category,
              'saleList'=> $sale_list
        ))->withErrors($validator);

      }

  }

    function BB_DSL(){

      if(Auth::check()){
      $input    = Request::all();
      $category = $input['category'];
      $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
      $test=json_decode($sale_list,true);

      if(!empty($test)){
         return view('user/BB_DSL',array(
        'saleList' => $sale_list,
        'category' => $category
      ));

      }
      else
      {
      return view('user/BB_DSL',array(
        'category'  => $category
      ));
      }

      } 
      else{

      return view('user/login');

      }
      
    }

    function BB_DSL_Del(){
      $input    = Request::all();
      $id       = $input['id'];
      $DSL_Success = "Delete Data Successful!";
      $category = $input['category'];
      users::del_sales($id);
      $sale_list = DB::table('sales')->where('type',$category)->orderBy('updated_at','dec')->get();
      return view('user/BB_DSL',array(
          'saleList'    => $sale_list,
          'category'    => $category,
          'DSL_Success' => $DSL_Success
        ));
    }

    function BB_VSL(){

      if(Auth::check()){
      $input    = Request::all();
      $category = $input['category'];
      $sale_list = DB::table('sales')->orderBy('updated_at','dec')->get();
      $test=json_decode($sale_list,true);

      if(!empty($test)){
         return view('user/VSL',array(
        'saleList' => $sale_list,
        'category' => $category
      ));

      }
      else
      {
      return view('user/VSL',array(
        'category'  => $category
      ));
      }

      } 
      else{

      return view('user/login');

      }
    }
}

?>