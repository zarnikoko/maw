<!DOCTYPE html>
<html lang="en">
<head>
</head>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<style>
.container-fluid{
  height:100vh;
}
</style>
<meta charset="UTF-8">
<title>Maw</title>
<body class="bg-light">
  <!-- <h1> Banking System </h1> -->
@if(count($errors)>0)
<div class="alert alert-danger errors fixed-top">
  <ul>
    @foreach ($errors->all() as $err)
      <li> {{ $err }} </li>
    @endforeach
  </ul>
</div>
@endif
@if(session('info'))
<div class="alert alert-success info fixed-top">
  {{session('info')}}
</div>
@endif
<div class="container-fluid">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-4 border shadow p-4 d-flex flex-column align-items-center"> 
            <h4 class="mb-5"> Register New User </h4>
                <div class="login">
                    <div class="form-group">
                        <form method="post" action="{{ URL::to('user/register') }}">
                           {{ csrf_field()  }}
                            <label for="username">UserName</label>
                            <input type="text" name="username" class="form-control mb-3" 
                            id="accno" required placeholder="Enter UserName">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control mb-3" 
                            id="password" required placeholder="Password">
                            <label for="password">Password Again</label>
                            <input type="password" name="passwordAgain" class="form-control mb-4" id="password" required placeholder="Password Confirm">

                    </div>
                    <!-- <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Remember me</label>
                    </div> -->
                            <input type="submit" class="btn btn-primary" value="Create User">
                            <a href="{{URL::to('user/login') }}" style="color:green"> 
                            Back to Login 
                            </a>
                          </form>
                    </div>
              </div>
       </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
    $(".errors").show();
    $(".info").show();

    $("#accno").click(function(){
      $(".errors").hide();
      $(".info").hide();
    });

    $("#password").click(function(){
      $(".errors").hide();
      $(".info").hide();
    });
	});
</script>