<!-- <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.dataTables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/datatable.min.css') }}">
<link rel="stylesheet" type="text/css" 
href="{{ URL::asset('css/jquery.dataTables_themeroller.css') }}">
<link href="{{ URL::asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery-ui.theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery-ui.structure.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
<link rel="icon" type="image/png" href="{{URL::asset('images/logo-icon.png')}}">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('all.css') }}">
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/datatable.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui.js') }}"></script>
<script src="{{ URL::asset('js/exchange_rate.js') }}"></script>
<script src="{{ URL::asset('js/select2.min.js') }}"></script>
<script src="{{ URL::asset('js/all.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::asset('js/contact.js') }}"></script>
<style>
.container-fluid{
   height:100vh;
}
.p{
 /* margin-left:5px;
  margin-right:5px;*/
  margin:auto;
  height:180px;
  overflow:hidden;
  border-radius:20px
}
.poi{
  cursor:pointer;
}
.w{

  color:white;
}
.nav-link{
    cursor:pointer;
}
.ho{
  color:white;
}
.cathe_label{

  font-weight:bolder;
  cursor:pointer;
}
.sticky {
  position: -webkit-sticky;
  position: sticky;
  top:41px;
  height:40px;
  background-color:white;
  z-index:2;
}
.sticky-thead{
  position: -webkit-sticky;
  position: sticky;
  top: 87px;
  background-color:white;
  z-index:2;
}
.y{
  color:yellow;
}
/*#BB{
  background: url('../images/BB_test.png');
  background-repeat: no-repeat;
  background-size: 97px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}

#BD{
  background: url('../images/BD_test.png');
  background-repeat: no-repeat;
  background-size: 140px 167px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}

#MJ{
  background: url('../images/MJ_test.png');
  background-repeat: no-repeat;
  background-size:100px 167px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#DB{
  background: url('../images/DB_test.png');
  background-repeat: no-repeat;
  background-size: 97px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#DD{
  background: url('../images/DD_test.png');
  background-repeat: no-repeat;
  background-size: 97px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#DF{
   background: url('../images/foreign.png');
  background-repeat: no-repeat;
  background-size: 97px 160px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
 -moz-border-radius: 100px;
}
#MC{
  background: url('../images/MC_test.png');
  background-repeat: no-repeat;
  background-size: 110px 180px;;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}

#MF{
  background: url('../images/license.png');
  background-repeat: no-repeat;
  background-size: 100px 110px;
  background-position: center center;
 -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#MS{
  background: url('../images/beauty.png');
  background-repeat: no-repeat;
  background-size: 120px 150px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}*/
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
body{
 font-family: 'Padauk', sans-serif;
}
.dropdown-menu{
  z-index:2000;
}
@page{
  margin-top:25mm;
  margin-bottom:25mm;
}

</style>
<meta charset="UTF-8">
<title> Maw </title>
<body class="bg-light">
@if(count($errors))
  <div class="alert alert-danger error">
    <ul>
        @foreach($errors->all() as $err)
          <li> {{ $err }} </li>
        @endforeach
    </ul>
  </div>
@endif

 <div class="container-fluid">
  <div class="row justify-content-right">
    <div class="col-7">
      <button  class="btn btn-primary pl-4 pr-4" onclick="window.location.href='{{URL::to('user/home')}}'" style="top:0;">
       <i class="fas fa-home"></i>
      </button>
      <img src="../images/mawlogo.png" width="70" height="70" class="ml-2">
          <label class="mt-3" style="cursor:default;color:#191970;font-size:20px;">
              <b>MAW Beauty Salon & Photo Studio</b>
          </lebel>
    </div>
    <div class="col-2 mt-3" id="cat_title">
      <button  class="btn btn-success pl-5 pr-5" style="float:left;cursor:default;">
        {{ $category }}
         </button>
    </div>
    <div class="col-3 mt-3">
      <div class="dropdown" style="float:right;">
           <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" 
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="fas fa-align-justify"></i>
                 </button>  
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                
                  <a class="dropdown-item" href="{{URL::to('user/logout')}}">
                  <i class="fa fa-door-open"></i>
                      Log out
                  </a>
                <a class="dropdown-item" id="contact">
                <i class="fa fa-book-open"></i>
                Contacts
                  </a>
            </div>
        </div>
      </div>
  </div>
  <div id="contactpopup" style="display:none;z-index:999999;overflow-y:hidden;" class="justify-content-center">
            <center> 
                    <h3 style="background-color:#32CD32;color:white"> 
                        ICT for Development Team <br>
                        Global Links Asia Myanmar Co., Ltd. 
                    </h3>
                <div class="bg-light pb-3">
                    <br>
                    <a href="http://www.ict4development.asia" class="contact-link text-primary">
                        www.ict4development.asia
                    </a>  
                    <br>
                    <a href="http://www.glamgroup.biz" class="contact-link text-primary">www.glamgroup.biz</a>
                    <br>
                    <a href="http://www.globallinks21.com" class="contact-link text-primary">www.globallinks21.com</a>
                    <br><br>
                    <i class="fas fa-envelope"></i> manager@ict4development.asia<br>
                    <i class="fas fa-phone"></i> 09-448015325
                </div>
            </center>
  </div>