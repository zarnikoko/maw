
<!DOCTYPE html>
<html lang="en">
<head>
</head>
@include("user/common")
    <div class="row justify-content-center mt-1">
        <div class="col-3" style="height:580px;border:2px solid blue">
            <div class="row pb-2 pt-2">
                <div class="col border bg-primary p poi btn w cathe"  id="BB">
                    <img src="../images/BB.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label" >
                        <span>အလှူမင်္ဂလာ </span>
                        <span style="border-bottom:2px solid lightgrey"><br>အလှမိတ်ကပ်</span>
                        <span class="y"> <br> BB </span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="BD">
                    <img src="../images/BD.jpg" width="90" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span style="border-bottom:2px solid lightgrey"><br>ဘွဲ့မိတ်ကပ်</span>
                        <span class="y"> <br> BD </span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="MJ">
                   <img src="../images/j.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span style="border-bottom:2px solid lightgrey"><br>ရတနာ</span>
                        <span class="y"> <br> MJ </span>
                    </label>
                </div>
            </div>
            <div class="row pb-2">
                <div class="col border bg-primary p poi btn w cathe"  id="DB">
                    <img src="../images/DB.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span> အလှူမင်္ဂလာ<br></span>
                        <span style="border-bottom:2px solid lightgrey">ဝတ်စုံ </span>
                        <span class="y"> <br> DB </span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="DD">
                    <img src="../images/DD.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span> ဘွဲ့ဝတ်စုံ</span>
                        <span style="border-bottom:2px solid lightgrey">ပြည်တွင်း </span>
                        <span class="y"> <br> DD </span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="DF">
                    <img src="../images/fconvo.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span> နိုင်ငံခြားဘွဲ့</span>
                        <span style="border-bottom:2px solid lightgrey">ဝတ်စုံ </span>
                        <span class="y"> <br> DF </span>
                    </label>
                </div>
            </div>
            <div class="row pb-2">
                <div class="col border bg-primary p poi btn w cathe"  id="MC">
                    <img src="../images/MC.png" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span>&nbsp;ကား <br></span>
                        <span style="border-bottom:2px solid lightgrey">လက်မှတ် </span>
                        <span class="y"> <br>MC</span>
                   </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="MF">
                    <img src="../images/f.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span>ပုံ၊ဘောင်၊</span>
                        <span style="border-bottom:2px solid lightgrey">လိုင်စင် </span>
                        <span class="y"> <br>MF</span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="MS">
                    <img src="../images/beauty2.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span>Beauty<br></span>
                        <span style="border-bottom:2px solid lightgrey">&nbsp;Salon </span>
                        <span class="y"> <br>MS</span>
                    </label>
                </div>
            </div>
       </div>
        <div class="col-9" style="height:580px;border:2px solid blue;border-left:none;overflow-y:auto;">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a class="nav-link  active" id="a_ASL">
                အရောင်းစာရင်းအပ်ရန်
                <form method="post" action="{{URL::to('user/ASL')}}" id="a_ASL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden" name="category" class="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_PS">​
                ငွေပေး၊ငွေချေရန်
                <form method="post" action="{{URL::to('user/PS')}}" id="a_PS_Form">
                  {{ csrf_field()  }}
                    <input type="hidden" name="category" class="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_DSL">
                စာရင်းဖျက်ရန်
                <form method="post" action="{{URL::to('user/DSL')}}" id="a_DSL_Form">
                  {{ csrf_field()  }}
                    <input type="hidden" name="category" class="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_VSL">
                စာရင်းချုပ်ကြည့်ရန်
                <form method="post" action="{{URL::to('user/VSL')}}" id="a_VSL_Form">
                  {{ csrf_field()  }}
                    <input type="hidden" name="category" class="category">
                </form>
              </a>
                </li>
            </ul>
            <div class="container" style="background-color:white;border-left:1px solid #dfdfdf;border-right:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf">
                <div style="height:20px"></div>
                <form action="{{URL::to('user/BB_ASL')}}" method="post" id="add_form">
                    {{ csrf_field()  }}
                    @if(!isset($input))
                        <div class="form-group row">
                            <label for="receipt_no" class="col-sm-2 col-form-label ml-3">
                                ဘောင်ချာနံပါတ်
                            </label>
                            <input type="number" name="receipt_no" class="form-control col-4 err" required 
                            placeholder="ဂဏန်းခြောက်လုံးရိုက်ထည့်ပါ (eg.000001)" autocomplete="off" 
                            id="receipt_no">        
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label ml-3">
                                အမည်
                            </label>
                            <input type="text" name="name" class="form-control col-4 err" required 
                            placeholder="အမည်" autocomplete="off">      
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-2 col-form-label ml-3">
                                လိပ်စာ
                            </label>
                            <input type="text" name="address" class="form-control col-4 err" required placeholder="လိပ်စာ"
                            autocomplete="off">     
                        </div>
                        <div class="form-group row">
                            <label for="phno" class="col-sm-2 col-form-label ml-3">
                                ဖုန်းနံပါတ်
                            </label>
                            <input type="text" name="phno" id="p_no" class="form-control col-4 err" required placeholder="ဖုန်းနံပါတ်" autocomplete="off">      
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-2 col-form-label ml-3">
                                နေ့စွဲ
                            </label>
                            <input type="text" name="date" class="form-control col-4 err" required placeholder="နေ့စွဲ" id="date" autocomplete="off">       
                        </div>
                        <div class="form-group row">
                            <label for="cmoney" class="col-sm-2 col-form-label ml-3">
                                သင့်ငွေ
                            </label>
                            <input type="number" name="cmoney" class="form-control col-4 err" required placeholder="သင့်ငွေ" id="cmoney" autocomplete="off">        
                        </div>
                        <div class="form-group row">
                            <label for="a_pay" class="col-sm-2 col-form-label ml-3">
                                စရံ
                            </label>
                            <input type="number" name="a_pay" class="form-control col-4 err" required placeholder="စရံ" id="a_pay" autocomplete="off">      
                        </div>
                        <div class="form-group row">
                            <label for="r_b" class="col-sm-2 col-form-label ml-3">
                                ကျန်ငွေ
                            </label>
                            <input type="number" name="r_b" class="form-control col-4 err" required placeholder="ကျန်ငွေ" readonly style="background-color:white"
                            id="r_b" autocomplete="off">
                        </div>
                        <input type="hidden" id="clone_receipt_no" name="receipt_number">
                    @endif
                    @if(isset($input))
                        <div class="form-group row">
                            <label for="receipt_no" class="col-sm-2 col-form-label ml-3">
                                ဘောင်ချာနံပါတ်
                            </label>
                            <input type="number" name="receipt_no" class="form-control col-4 err" required 
                            placeholder="ဂဏန်းလေးလုံးရိုက်ထည့်ပါ (eg.0001)" autocomplete="off" id="receipt_no" 
                            value="{{$input['receipt_no']}}">     
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label ml-3">
                                အမည်
                            </label>
                            <input type="text" name="name" class="form-control col-4 err" required 
                            placeholder="အမည်" autocomplete="off" value="{{$input['name']}}">       
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-2 col-form-label ml-3">
                                လိပ်စာ
                            </label>
                            <input type="text" name="address" class="form-control col-4 err" required placeholder="လိပ်စာ" autocomplete="off" value="{{$input['address']}}">     
                        </div>
                        <div class="form-group row">
                            <label for="phno" class="col-sm-2 col-form-label ml-3">
                                ဖုန်းနံပါတ်
                            </label>
                            <input type="text" name="phno" class="form-control col-4 err" required placeholder="ဖုန်းနံပါတ်"  id="p_no" autocomplete="off" value="{{$input['phno']}}">     
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-sm-2 col-form-label ml-3">
                                နေ့စွဲ
                            </label>
                            <input type="text" name="date" class="form-control col-4 err" required placeholder="နေ့စွဲ" id="date" autocomplete="off" value="{{$input['date']}}">     
                        </div>
                        <div class="form-group row">
                            <label for="cmoney" class="col-sm-2 col-form-label ml-3">
                                သင့်ငွေ
                            </label>
                            <input type="number" name="cmoney" class="form-control col-4 err" required placeholder=" သင့်ငွေ" id="cmoney" autocomplete="off" value="{{$input['cmoney']}}">       
                        </div>
                        <div class="form-group row">
                            <label for="a_pay" class="col-sm-2 col-form-label ml-3">
                                စရံ
                            </label>
                            <input type="number" name="a_pay" class="form-control col-4 err" required placeholder="စရံ" id="a_pay" autocomplete="off" value="{{$input['a_pay']}}">      
                        </div>
                        <div class="form-group row">
                            <label for="r_b" class="col-sm-2 col-form-label ml-3">
                                ကျန်ငွေ
                            </label>
                            <input type="number" name="r_b" class="form-control col-4 err" required placeholder="ကျန်ငွေ" readonly style="background-color:white"
                            id="r_b" autocomplete="off" value="{{$input['r_b']}}">
                        </div>
                        <input type="hidden" id="clone_receipt_no" name="receipt_number" 
                        value="{{$input['receipt_number']}}">
                    @endif
                         <input type="submit" value="ဒေတာထည့်ရန်" class="btn btn-primary mb-4" 
                         style="margin-left:150px">
                         <input type="button" value="clear" class="btn btn-warning mb-4" id="clear_add_form">
                         <input type="hidden" class="category" name="category" id="category">
                </form>
            </div>
        </div>
    </div>
 </div>
</body>
<script>
    $(document).ready(function(){

        $("#p_no").on('input propertychange paste keypress',function(evnet){
            var minus       = event.keyCode;
                if(minus==48 || minus==49 || minus ==50 || minus ==51 || minus ==52 || minus ==53 || minus ==54 || minus ==55 || minus ==56 || minus ==57 ){
                }
                else{
                    event.preventDefault();
                }
        });

        var today    = new Date();
        var dd       =  today.getDate();
        var mm       =  today.getMonth()+1;
        var yyyy     =  today.getFullYear();
        if(dd<10)
        dd= '0'+dd;
       if(mm<10)
        mm= '0'+mm;
        today       =  dd+'-'+mm+'-'+yyyy;
        // $(".w label").css("background-color","rgba(0,0,0,0.1)");
        // $(".w").mouseenter(function(){
        //     $(this).css("color","white");
        //     $('label',this).css("background-color","rgba(0,0,0,0.2)");
        // }).mouseleave(function(){
        //     $('label',this).css("background-color","rgba(0,0,0,0.1)");

        // });

          $(".err").click(function(){
                $(".error").hide();
          });

           $(".category").val("{{$category}}");
          $("#date").val(today);
          $("#date").datepicker({
            dateFormat : "dd-mm-yy",
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: $("#date").offset().top-15,
                        left: $("#date").offset().left
                    });
                }, 0);
            }
          }).attr("readonly","true").css("background-color","white");
 
          $("#cmoney").on('input propertychange keypress',function(event){
                var a_pay = $("#a_pay").val();
                var minus       = event.keyCode;
                if(minus==48 || minus==49 || minus ==50 || minus ==51 || minus ==52 || minus ==53 || minus ==54 || minus ==55 || minus ==56 || minus ==57 ){
                }
                else{
                    event.preventDefault();
                }
                var cmoney= $("#cmoney").val();
                if(a_pay==""){
                    a_pay=0;
                }
                var r_b = cmoney-a_pay;
                $("#r_b").val(r_b);
                if(r_b<0){
                    alert("ကျန်ငွေသည်အနှုတ်မဖြစ်ရပါ ။");
                    $("#r_b").val("");
                    $(this).val("");
                    $("#a_pay").val("");
                }
                if(cmoney==""){
                $("#r_b").val("");  
                }
          }); 

            $('#cmoney').bind("paste",function(e) {
                e.preventDefault();
            });

            $("#a_pay").on('input propertychange keypress',function(event){
                var a_pay        = $("#a_pay").val();
                var minus       = event.keyCode;
                    if(minus==48 || minus==49 || minus ==50 || minus ==51 || minus ==52 || minus ==53 || minus ==54 || minus ==55 || minus ==56 || minus ==57 ){
                    }
                    else{
                        event.preventDefault();
                    }
                var cmoney= $("#cmoney").val();
                var r_b = cmoney-a_pay;
                $("#r_b").val(r_b);
                if(r_b<0){
                    alert("ကျန်ငွေသည်အနှုတ်မဖြစ်ရပါ ။");
                    var len = a_pay.length;
                    a_pay = a_pay.slice(0,len-1);
                    $(this).val(a_pay);
                    var a_pay = $("#a_pay").val();
                    var cmoney= $("#cmoney").val();
                    var r_b = cmoney-a_pay;
                     $("#r_b").val(r_b);
                }
                if(cmoney==""){
                $("#r_b").val("");  
                }
          }); 

            $('#a_pay').bind("paste",function(e) {
                e.preventDefault();
            });

            $("#a_pay").click(function(){
                var cmoney= $("#cmoney").val();
                if(cmoney==""){
                    alert("သင့်ငွေတွင်ဒေတာအရင်ရိုက်ထည့်ပါရန်။");
                    $(this).val("");
                    $("#cmoney").focus();
                }
            });

            $("#receipt_no").on('input propertychange keypress',function(event){
               var category= $("#category").val();
               var receipt = $("#receipt_no").val();
                var minus       = event.keyCode;
                    if(minus==48 || minus==49 || minus ==50 || minus ==51 || minus ==52 || minus ==53 || minus ==54 || minus ==55 || minus ==56 || minus ==57 ){
                    }
                    else{
                        event.preventDefault();
                    }
               if(receipt.length>6){
                alert("ဂဏန်းအလုံးရေ ၆လုံးသာလက်ခံပါသည်");
                receipt = receipt.slice(0,6);
                $("#receipt_no").val(receipt);
               }
               // to change MAW 
               var test    = "MAW-"+category+"-"+receipt;
               $("#clone_receipt_no").val(test);
               // to change MAW 
            });

            $("#receipt_no").focusout(function(){
                var receipt = $("#receipt_no").val();
                    if(receipt.length<6 && receipt !="" ){
                        alert("ဘောင်ချာဂဏန်းအလုံးရေ၆လုံးမပြည့်သေးပါ");
                    }
            });

            $("#{{$category}}").removeClass("bg-primary");
            $("#{{$category}}").addClass("bg-success");

        $("#a_ASL").click(function(){
            $("#a_ASL_Form").submit();
        });

        $("#a_PS").click(function(){
            $("#a_PS_Form").submit();
        });

        $("#a_DSL").click(function(){
            $("#a_DSL_Form").submit();
        });

        $("#a_VSL").click(function(){
            $("#a_VSL_Form").submit();
        });

        $(".cathe").click(function(){
            $(".cathe").removeClass("bg-success");
            $(".cathe").addClass("bg-primary");
            $(this).removeClass("bg-primary");
            $(this).addClass("bg-success");
            var c_value  = this.id;
            $(".category").val(c_value);
            $("#cat_title button").html(c_value);
            
               var category= $("#category").val();
               var receipt = $("#receipt_no").val();
               // to change MAW 
               var test    = "MAW-"+category+"-"+receipt;
               $("#clone_receipt_no").val(test);
               // to change MAW 
         });

        // clear form //
            $("#clear_add_form").click(function(){
                $("#add_form").trigger("reset");
                $("#clone_receipt_no").val("");
            });
        // clear form //

        <? if (isset($ASL_Success)) : ?>
        alert("ဒေတာထည့်မှုအောင်မြင်သည်။");
        <? endif; ?>

        <? if(count($errors)): ?>
            alert("ဒေတာထည့်မှုမအောင်မြင်ပါ။");
       <?  endif;   ?>

    });
</script>
</html>