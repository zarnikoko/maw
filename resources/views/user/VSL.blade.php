
<!DOCTYPE html>
<html lang="en">
<head>
</head>
@include("user/common")
  <div class="row justify-content-center mt-1">
        <div class="col-3" style="height:580px;border:2px solid blue">
            <div class="row pb-2 pt-2">
                <div class="col border bg-primary p poi btn w cathe"  id="BB">
                    <img src="../images/BB.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label" >
                        <span>အလှူမင်္ဂလာ </span>
                        <span style="border-bottom:2px solid lightgrey"><br>အလှမိတ်ကပ်</span>
                        <span class="y"> <br> BB </span>
                    </label>
                </div>

                <div class="col border bg-primary p poi btn w cathe"  id="BD">
                  <img src="../images/BD.jpg" width="90" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span style="border-bottom:2px solid lightgrey"><br>ဘွဲ့မိတ်ကပ်</span>
                        <span class="y"> <br> BD </span>
                    </label>
                </div>

                <div class="col border bg-primary p poi btn w cathe"  id="MJ">
                   <img src="../images/j.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span style="border-bottom:2px solid lightgrey"><br>ရတနာ</span>
                        <span class="y"> <br> MJ </span>
                    </label>
                </div>
            </div>
            <div class="row pb-2">
                <div class="col border bg-primary p poi btn w cathe"  id="DB">
                    <img src="../images/DB.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                        <span>
                            အလှူမင်္ဂလာ<br>
                        </span>
                        <span style="border-bottom:2px solid lightgrey">
                        ဝတ်စုံ
                        </span>
                        <span class="y"> <br> DB </span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="DD">
                    <img src="../images/DD.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                    <span>
                        ဘွဲ့ဝတ်စုံ
                    </span>
                    <span style="border-bottom:2px solid lightgrey">
                        ပြည်တွင်း
                    </span>
                        <span class="y"> <br> DD </span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="DF">
                    <img src="../images/fconvo.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                    <span>
                        နိုင်ငံခြားဘွဲ့
                    </span>
                    <span style="border-bottom:2px solid lightgrey">
                        ဝတ်စုံ
                    </span>
                        <span class="y"> <br> DF </span>
                    </label>
                </div>
            </div>
            <div class="row pb-2">
                <div class="col border bg-primary p poi btn w cathe"  id="MC">
                   <img src="../images/MC.png" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                     <span>
                      &nbsp;ကား <br>
                     </span>
                     <span style="border-bottom:2px solid lightgrey">
                       လက်မှတ်
                     </span>
                       <span class="y"> <br>MC</span>
                   </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="MF">
                   <img src="../images/f.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                      <span>
                       ပုံ၊ဘောင်၊
                      </span>
                      <span style="border-bottom:2px solid lightgrey">
                        လိုင်စင်
                      </span>
                       <span class="y"> <br>MF</span>
                    </label>
                </div>
                <div class="col border bg-primary p poi btn w cathe"  id="MS">
                    <img src="../images/beauty2.jpg" width="70" height="90" class="rounded-circle">
                    <label class="cathe_label">
                      <span>
                      Beauty<br>
                      </span>
                      <span style="border-bottom:2px solid lightgrey">
                       &nbsp;Salon
                      </span>
                      <span class="y"> <br>MS</span>
                    </label>
                </div>
            </div>
       </div>
    <div class="col-9" style="height:580px;border:2px solid blue;border-left:none;overflow-y:hidden;" 
        id="list_main_container">
      <ul class="nav nav-tabs nav-justified sticky-top">
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_ASL">
                အရောင်းစာရင်းအပ်ရန်
                <form method="post" action="{{URL::to('user/ASL')}}" id="a_ASL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden" name="category" class="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_PS">​
                ငွေပေး၊ငွေချေရန်
                <form method="post" action="{{URL::to('user/PS')}}" id="a_PS_Form">
                    {{ csrf_field()  }}
                    <input type="hidden"  name="category" class="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-primary" style="color:white" id="a_DSL">
                စာရင်းဖျက်ရန်
                <form method="post" action="{{URL::to('user/DSL')}}" id="a_DSL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden"  name="category" class="category">
                </form>
              </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="a_VSL">
                စာရင်းချုပ်ကြည့်ရန်
                <form method="post" action="{{URL::to('user/VSL')}}" id="a_VSL_Form">
                    {{ csrf_field()  }}
                    <input type="hidden"  name="category" class="category">
                </form>
              </a>
                </li>
            </ul>
        <div class="container" style="background-color:white;border-left:1px solid #dfdfdf;border-right:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf">

                <!-- for category heading -->
          <div style="height:40px;z-index:3;background-color:transparent;" class="row" 
                id="cathe_heading">
                    <div class="col-4>" style="background-color:white" >
                        <input type="button" class="btn btn-secondary mt-1" value="Back" 
                        style="float:left;display:none;" id="back">
                    </div>
                    <div class="col-6" style="background-color:white;">
                        <h1 class="col-md-auto btn bg-info font-weight-bold mt-1" 
                        style="color:white;display:none;cursor:default;float:right" id="category_heading">
                        </h1>
                    </div>
                    <div class="col-2" style="background-color:white;">
                    </div>      
                </div>
                <!-- for category heading -->

<!--for list view divs -->
                <div style="height:480px"  id="list_view_container">
                    <div class="row justify-content-center" style="height:200px">
                        <div class="col border ml-3 mt-3 mr-1 bg-info btn shadow ho" id="daily">
                            <h5>
                                နေ့အလိုက်စာရင်း
                            </h5>
                           <img src="../images/date11.png" width="160" height="138">
                        </div>
                        <div class="col border ml-2 mt-3 mr-2 bg-info btn shadow ho" id="monthly">
                             <h5>
                                လအလိုက်စာရင်း
                            </h5>
                             <img src="../images/month.jpg" width="140" height="128">
                        </div>
                    </div>
                    <div class="row justify-content-center" style="height:200px;">
                        <div class="col border ml-3 mt-3 mr-1 bg-info btn shadow ho" id="yearly">
                            <h5>
                                နှစ်အလိုက်စာရင်း
                            </h5>
                             <img src="../images/year2.png" width="140" height="128">
                        </div>
                        <div class="col border ml-2 mt-3 mr-2 bg-info btn shadow ho" id="from_to">
                            <h5>
                               From_To
                            </h5>
                             <img src="../images/from_to_date.png" width="140" height="128">
                        </div>
                    </div>
                </div>
<!--for list view divs -->

<!-- specific forms -->
                <div id="form_view_container" class="pt-5 font-weight-bold" style="display:none">
                 <center>

                   <select class="form-control col-4 mb-4" id="cathe_sel">
                        <option id="cathe_opt"></option>
                        <option id="all" value="all">All</option>
                    </select>

                    <input type="text" id="date" name="date" class="form-control col-4 mb-4" placeholder="Date"
                    style="cursor:default;display:none" autocomplete="off">

                    <select class="form-control col-4 mb-4" id="month_sel" style="display:none">

                      <? for($i=1;$i<13;$i++) {  ?>
                         <?   if($i<10):  ?>
                        <option value="0{{$i}}" id="0{{$i}}"></option>
                         <?   endif; ?>
                          <?   if($i>10 || $i==10):  ?>
                        <option value="{{$i}}" id="{{$i}}"></option>
                         <?   endif; ?>
                      <?   }  ?>

                    </select>

                     <select class="form-control col-4 mb-4" id="year_sel" style="display:none">

                      <? 
                         $year = 2018;
                         for($i=0;$i<14;$i++) {  ?>
                         <option value="{{$year}}" id="{{$year}}">{{$year}}</option>
                         <? $year = $year +1; ?>  
                      <?   }  ?>

                    </select>

                    <input type="text" id="from" name="from" class="form-control col-4 mb-4" placeholder="From"
                    style="cursor:default;display:none;" autocomplete="off">

                    <input type="text" id="to" name="to" class="form-control col-4 mb-4" placeholder="To"
                    style="cursor:default;display:none;" autocomplete="off">

                    <input type="button" class="btn btn-primary mt-3 mb-4" value="ရှာမည်" id="btn_daily" 
                    style="display:none">
                    <input type="button" class="btn btn-primary mt-3 mb-4" value="ရှာမည်" id="btn_monthly"
                    style="display:none">
                    <input type="button" class="btn btn-primary mt-3 mb-4" value="ရှာမည်" id="btn_yearly"
                    style="display:none">
                    <input type="button" class="btn btn-primary mt-3 mb-4" value="ရှာမည်" id="btn_from_to"
                    style="display:none">

                 </center>
                </div>
<!-- specific forms -->
               <div id="bouncher" class="text-info sticky" style="display:none;background-color:white;height:50px">
                    <label style="float:right;cursor:default;border:1px solid black;" 
                    class="bg-light btn pl-1 pr-1 mt-1">
                        ဘောင်ချာအရေအတွက် <span id="receipt_count"></span>
                    </label>
               </div> 
               <div id="result_table" class="mb-5" style="display:none">
               </div>
      </div>
    </div>
    </div>
 </div>
</body>
<script>
  $(document).ready(function(){
           var today    = new Date();
           var mm       =  today.getMonth()+1;
           var yyyy     =  today.getFullYear();
           if(mm<10)
            mm= '0'+mm;

          $("#01").html("ဇန်နဝါရီ - 01");
          $("#02").html("ဖေဖော်ဝါရီ - 02");
          $("#03").html("မတ် - 03");
          $("#04").html("ဧပြီ - 04");
          $("#05").html("မေ - 05");
          $("#06").html("ဇွန် - 06");
          $("#07").html("ဇူလိုင် - 07");
          $("#08").html("ဩဂုတ် - 08");
          $("#09").html("စက်တင်ဘာ - 09");
          $("#10").html("အောက်တိုဘာ - 10");
          $("#11").html("နိုဝင်ဘာ - 11");
          $("#12").html("ဒီဇင်ဘာ - 12");

        // $(".w label").css("background-color","rgba(0,0,0,0.1)");

        // $(".w").mouseenter(function(){
        //     $(this).css("color","white");
        //     $('label',this).css("background-color","rgba(0,0,0,0.2)");
        // }).mouseleave(function(){
        //     $('label',this).css("background-color","rgba(0,0,0,0.1)");

        // });

          $(".ho").mouseenter(function(){
            $(this).css("color","black");
          }).mouseleave(function(){
            $(this).css("color","white");
          });

          $(".category").val("{{$category}}");

          $("#{{$category}}").removeClass("bg-primary");
          $("#{{$category}}").addClass("bg-success");

        $(".err").click(function(){
                $(".error").hide();
          });
  
        $("#a_ASL").click(function(){
            $("#a_ASL_Form").submit();
        });

        $("#a_PS").click(function(){
            $("#a_PS_Form").submit();
        });

        $("#a_DSL").click(function(){
            $("#a_DSL_Form").submit();
        });

         $("#a_VSL").click(function(){
            $("#a_VSL_Form").submit();
        });

         $(".cathe").click(function(){
            $(".cathe").removeClass("bg-success");
            $(".cathe").addClass("bg-primary");
            $(this).removeClass("bg-primary");
            $(this).addClass("bg-success");
            var c_value  = this.id;
            $(".category").val(c_value);
            $("#cat_title button").html(c_value);
            $("#cathe_opt").html(c_value);
            $("#cathe_opt").val(c_value);
            $("#cathe_opt").attr("selected");
            $("#all").attr("selected",false);
            $("#result_table").hide();
            $("#bouncher").hide();
         });

         $("#daily").click(function(){
            show_hide();
            $("#category_heading").html("နေ့အလိုက်စာရင်း");
            $("#date").show();
            $("#btn_daily").show();
         });

          $("#monthly").click(function(){
            show_hide();
            $("#category_heading").html("လအလိုက်စာရင်း");
            $("#month_sel").show();
            $("#"+mm).attr("selected","true");
            $("#btn_monthly").show();
         });

          $("#yearly").click(function(){
            show_hide();
            $("#category_heading").html("နှစ်အလိုက်စာရင်း");
            $("#year_sel").show();
            $("#"+yyyy).attr("selected","true");
            $("#btn_yearly").show();    
         });

          $("#from_to").click(function(){
            show_hide();
            $("#category_heading").html("From_To");
            $("#from").show();
            $("#to").show();
            $("#btn_from_to").show();
         });

          $("#back").click(function(){
            $(this).hide();
            $("#category_heading").hide();
            $("#list_view_container").show();
            reset();
            $("#list_main_container").scrollTop(0);
            $("#list_main_container").css("overflow-y","hidden");
            $("#cathe_heading").removeClass("sticky");
          });

          $("#cathe_sel").change(function(){
                $("#result_table").hide();
          });
          $("#date").change(function(){
                $("#result_table").hide();
                $("#bouncher").hide();
          });

          $("#cathe_opt").change(function(){
                $("#result_table").hide();
                $("#bouncher").hide();
          });

          $("#month_sel").change(function(){
                 $("#result_table").hide();
                 $("#bouncher").hide();
          });

           $("#year_sel").change(function(){
                 $("#result_table").hide();
                 $("#bouncher").hide();
          });

          $("#btn_daily").click(function(){

            if($("#date").val()==""){
                    alert("နေ့စွဲတွင်ရွေးမထားပါ။");
            }
            else{

                var day   = $("#date").val();
                var start = 0;
                var n     = 10;
                var title = "နေ့အလိုက်စာရင်း";
                display_table(day,start,n,title);
            }

          });

          $("#btn_monthly").click(function(){
               var month = $("#month_sel").val();
               var start = 3;
               var n     = 2;
               var title = "လအလိုက်စာရင်း";
               display_table(month,start,n,title);

          });

          $("#btn_yearly").click(function(){
               var year = $("#year_sel").val();
               var start=6;
               var n    =4;
               var title = "နှစ်အလိုက်စာရင်း";
               display_table(year,start,n,title);
          });

          $("#btn_from_to").click(function(){
            if($("#from").val()=="" || $("#to").val()==""){
                    alert("နေ့စွဲနှစ်ခုလုံးတွင်ရွေးပေးပါ။");
                 
            }
            else{
                 
                "@if(isset($saleList))"
                var category = $("#cathe_sel").val();
                var from     = $("#from").val().split("-");
                var f        = new Date(from[2],from[1]-1,from[0]);
                var to       = $("#to").val().split("-");
                var t        = new Date(to[2],to[1]-1,to[0]);
                if(category=="all"){
                var html='';
                    html+='<table id="daily_table" class="display" cellspacing="0" width="100%">';
                    html+='<thead>';
                    html+='<tr>';
                    html+='<th class="sticky-thead">စဉ်</th>';
                    html+='<th class="sticky-thead">နေ့စွဲ</th>';
                    html+='<th class="sticky-thead">ဖုန်းနံပါတ်</th>';
                    html+='<th class="sticky-thead">ဘောင်ချာ</th>';
                    html+='<th class="sticky-thead">သင့်ငွေ</th>';
                    html+='<th class="sticky-thead">စရံ</th>';
                    html+='<th class="sticky-thead">ချေပြီးငွေ</th>';
                    html+='<th class="sticky-thead">ကျန်ငွေ</th>';
                    html+='<th class="sticky-thead">ငွေချေရက်</th>';
                    html+='</tr>';
                    html+='</thead>';
                    html+='<tbody>';
                    var sList = <?= json_encode($saleList) ?>;
                    var len = sList.length;
                    var len2 = 0;
                    var sum_cmoney=0;
                    var sum_apayment=0;
                    var sum_rbalance=0;
                    var sum_payamt=0;
                    for(var i=0;i<len;i++){
                      var sListdate = sList[i].date.split("-");
                      var sdate     = new Date(sListdate[2],sListdate[1]-1,sListdate[0]);
                      console.log(sdate);
                        if(sdate.getTime()==f.getTime()|| sdate.getTime()==t.getTime() || 
                            (sdate.getTime()>f.getTime() && sdate.getTime()<t.getTime())){
                        len2 = len2+1;
                        sum_cmoney= +sum_cmoney + +sList[i].c_money;
                        sum_apayment= +sum_apayment + +sList[i].a_payment;
                        sum_rbalance= +sum_rbalance + +sList[i].r_balance;
                        if(String(sList[i].pay_amt)=='null'){
                            sum_payamt = +sum_payamt + 0;
                        }
                        else{
                            sum_payamt = +sum_payamt + +sList[i].pay_amt;
                        }

                        html+='<tr>';
                        html+='<td>'+len2+'</td>';
                        html+='<td>'+sList[i].date+'</td>';
                        html+='<td>'+sList[i].phone+'</td>';
                        html+='<td>'+sList[i].receipt_no+'</td>';
                        html+='<td>'+sList[i].c_money+'</td>';
                        html+='<td>'+sList[i].a_payment+'</td>';
                        html+='<td>'+String(sList[i].pay_amt).replace('null','')+'</td>';
                        html+='<td>'+sList[i].r_balance+'</td>';
                        html+='<td>'+String(sList[i].pay_date).replace('null','')+'</td>';
                        html+="</tr>";
                        }
                    }
                    html+='</tbody>';
                    if(len2>0){

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td>စုစုပေါင်း</td>';
                    html+='<td>'+sum_cmoney+'</td>';
                    html+='<td>'+sum_apayment+'</td>';
                    html+='<td>'+sum_payamt+'</td>';
                    html+='<td>'+sum_rbalance+'</td>';
                    html+='<td></td>';
                    html+='</tr>';
                    html+='</tfoot>';

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th>စုစုပေါင်း</th>';
                    html+='<th>သင့်ငွေ</th>';
                    html+='<th>စရံ</th>';
                    html+='<th>ချေပြီးငွေ</th>';
                    html+='<th>ကျန်ငွေ</th>';
                    html+='<th></th>';
                    html+='</tr>';
                    html+='</tfoot>';
                    }
                    html+='</table>';
                    $("#result_table").html(html);
                    $("#daily_table").DataTable({
                       "bInfo"           : false,
                       "bLengthChange"   : false,
                       "aaSorting"       : [],
                       "bSort"           : false,
                       "bAutoWidth"      : false,
                        "bFilter"        : false,
                        "bPaginate"      : false,
                         dom: 'Bfrtip',
                        buttons: [
                          { extend: 'print',
                          footer: true,
                          title:'MAW - Photo Studio (From-To စာရင်း)',
                              customize: function ( win ) {
                                    $(win.document.body)
                                    .css( 'font-size', '14pt' );
                                    // .prepend(
                                    // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                                    // );
                                    $(win.document.body).find( 'table' )
                                    .addClass( 'compact' )
                                    .css( {'font-size': 'inherit','margin-top' : '50px'});
                                    }
                          }
                        ]
                      //"aLengthMenu"   : [5,10,15,20],
                      // "iDisplayLength": 5
                        
                      });
                    var table = $("#daily_table").DataTable();
                    var count = table.rows().count();
                    $("#receipt_count").html(count);
                    $("#result_table").show();
                    $("#bouncher").show();
                }

                else{

                var html='';
                    html+='<table id="daily_table" class="display" cellspacing="0" width="100%">';
                    html+='<thead>';
                    html+='<tr>';
                    html+='<th class="sticky-thead">စဉ်</th>';
                    html+='<th class="sticky-thead">နေ့စွဲ</th>';
                    html+='<th class="sticky-thead">ဖုန်းနံပါတ်</th>';
                    html+='<th class="sticky-thead">ဘောင်ချာ</th>';
                    html+='<th class="sticky-thead">သင့်ငွေ</th>';
                    html+='<th class="sticky-thead">စရံ</th>';
                    html+='<th class="sticky-thead">ချေပြီးငွေ</th>';
                    html+='<th class="sticky-thead">ကျန်ငွေ</th>';
                    html+='<th class="sticky-thead">ငွေချေရက်</th>';
                    html+='</tr>';
                    html+='</thead>';
                    html+='<tbody>';
                    var sList = <?= json_encode($saleList) ?>;
                    var len = sList.length;
                    var len2=0;
                    var sum_cmoney=0;
                    var sum_apayment=0;
                    var sum_rbalance=0;
                    var sum_payamt=0;
                    for(var i=0;i<len;i++){
                      var sListdate = sList[i].date.split("-");
                      var sdate     = new Date(sListdate[2],sListdate[1]-1,sListdate[0]);
                    if((sdate.getTime()==f.getTime() || sdate.getTime()==t.getTime() ||
                        (sdate.getTime() > f.getTime()&&sdate.getTime()<t.getTime()) ) && sList[i].type==category){
                        len2=len2+1;
                        sum_cmoney = +sum_cmoney + +sList[i].c_money;
                        sum_apayment= +sum_apayment + +sList[i].a_payment;
                        sum_rbalance= +sum_rbalance + +sList[i].r_balance;
                        if(String(sList[i].pay_amt)=='null'){
                            sum_payamt = +sum_payamt + 0;
                        }
                        else{
                            sum_payamt = +sum_payamt + +sList[i].pay_amt;
                        } 
                        html+='<tr>';
                        html+='<td>'+len2+'</td>';
                        html+='<td>'+sList[i].date+'</td>';
                        html+='<td>'+sList[i].phone+'</td>';
                        html+='<td>'+sList[i].receipt_no+'</td>';
                        html+='<td>'+sList[i].c_money+'</td>';
                        html+='<td>'+sList[i].a_payment+'</td>';
                        html+='<td>'+String(sList[i].pay_amt).replace('null','')+'</td>';
                        html+='<td>'+sList[i].r_balance+'</td>';
                        html+='<td>'+String(sList[i].pay_date).replace('null','')+'</td>';
                        html+="</tr>";
                        }
                    }
                    html+='</tbody>';
                    if(len2>0){

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td>စုစုပေါင်း</td>';
                    html+='<td>'+sum_cmoney+'</td>';
                    html+='<td>'+sum_apayment+'</td>';
                    html+='<td>'+sum_payamt+'</td>';
                    html+='<td>'+sum_rbalance+'</td>';
                    html+='<td></td>';
                    html+='</tr>';
                    html+='</tfoot>';

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th>စုစုပေါင်း</th>';
                    html+='<th>သင့်ငွေ</th>';
                    html+='<th>စရံ</th>';
                    html+='<th>ချေပြီးငွေ</th>';
                    html+='<th>ကျန်ငွေ</th>';
                    html+='<th></th>';
                    html+='</tr>';
                    html+='</tfoot>';

                    }
                    html+='</table>';
                    $("#result_table").html(html);
                    $("#daily_table").DataTable({
                       "bInfo"           : false,
                       "bLengthChange"   : false,
                       "aaSorting"       : [],
                       "bSort"           : false,
                       "bAutoWidth"    : false,
                        "bFilter"      : false,
                        "bPaginate"     : false, 
                        dom: 'Bfrtip',
                        buttons: [
                          { extend: 'print',
                          footer: true,
                          title:'MAW - Photo Studio (From-To စာရင်း)',
                              customize: function ( win ) {
                                    $(win.document.body)
                                    .css( 'font-size', '14pt' );
                                    // .prepend(

                                    // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                                    // );
                                    $(win.document.body).find( 'table' )
                                    .addClass( 'compact' )
                                    .css( {'font-size': 'inherit','margin-top' : '50px'});
                                    }
                          }
                        ]
                      //"aLengthMenu"   : [5,10,15,20],
                      // "iDisplayLength": 5
                      });
                    var table = $("#daily_table").DataTable();
                    var count = table.rows().count();
                    $("#receipt_count").html(count);
                    $("#result_table").show();
                    $("#bouncher").show();
                   
                }
            "@endif"

            "@if(!isset($saleList))"
               alert("You have no data yet");
            "@endif"
            }

          });

          $("#to").change(function(){
            var to = $("#to").val().split("-");
            var t  = new Date(to[2],to[1]-1,to[0]);
            var from = $("#from").val().split("-");
            var f  = new Date(from[2],from[1]-1,from[0]);

            if(t.getTime() < f.getTime() || t.getTime() == f.getTime()){
                alert("စမှတ်နေ့စွဲသည် ဆုံးမှတ်နေ့စွဲထက်မကြီးရပါ။မတူရပါ။");
                $(this).val("");
            }
             $("#result_table").hide();
                 $("#bouncher").hide();
          })

           $("#from").change(function(){
            var to = $("#to").val().split("-");
            var t  = new Date(to[2],to[1]-1,to[0]);
            var from = $("#from").val().split("-");
            var f  = new Date(from[2],from[1]-1,from[0]);

            if(t.getTime() < f.getTime() || t.getTime() == f.getTime()){
                alert("စမှတ်နေ့စွဲသည် ဆုံးမှတ်နေ့စွဲထက်မကြီးရပါ။မတူရပါ။");
                $(this).val("");
            }
             $("#result_table").hide();
                 $("#bouncher").hide();
          })

          $("#date").datepicker({
            dateFormat : "dd-mm-yy",
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: $("#date").offset().top-15,
                        left: $("#date").offset().left
                    });
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
          }).attr("readonly","true").css("background-color","white");

           $("#from").datepicker({
            dateFormat : "dd-mm-yy",
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: $("#from").offset().top-15,
                        left: $("#from").offset().left
                    });
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
          }).attr("readonly","true").css("background-color","white");

            $("#to").datepicker({
            dateFormat : "dd-mm-yy",
            beforeShow: function (input, inst) {
                setTimeout(function () {
                    inst.dpDiv.css({
                        top: $("#to").offset().top-15,
                        left: $("#to").offset().left
                    });
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
          }).attr("readonly","true").css("background-color","white");

            function reset(){
                $("#cathe_opt").attr("selected");
                $("#all").attr("selected",false);
                $("#date").val("");
                $("#date").hide();
                $("#year_sel").hide();
                $("#year_sel").val(yyyy).attr("selected");
                $("#month_sel").hide();
                $("#month_sel").val(mm).attr("selected");
                $("#from").val("");
                $("#to").val("");
                $("#from").hide();
                $("#to").hide();
                $("#btn_daily").hide();
                $("#btn_monthly").hide();
                $("#btn_yearly").hide();
                $("#btn_from_to").hide();
                $("#result_table").hide();
                $("#bouncher").hide();
                $("#cathe_sel").hide();
            }

            function show_hide(){

                $("#cathe_heading").addClass("sticky");
                $("#list_view_container").hide();
                $("#back").show();
                $("#category_heading").show();
                $("#form_view_container").show();
                $("#cathe_sel").show();
                $("#cathe_opt").val($(".category").val());
                $("#cathe_opt").html($(".category").val());
                $("#list_main_container").css("overflow-y","auto");
            }

            function display_table(d_m_y,start,n,title){
             "@if(isset($saleList))"
                var category = $("#cathe_sel").val();
                if(category=="all"){
                var html='';
                    html+='<table id="daily_table" class="display" cellspacing="0" width="100%">';
                    html+='<thead>';
                    html+='<tr>';
                    html+='<th class="sticky-thead">စဉ်</th>';
                    html+='<th class="sticky-thead">နေ့စွဲ</th>';
                    html+='<th class="sticky-thead">ဖုန်းနံပါတ်</th>';
                    html+='<th class="sticky-thead">ဘောင်ချာ</th>';
                    html+='<th class="sticky-thead">သင့်ငွေ</th>';
                    html+='<th class="sticky-thead">စရံ</th>';
                    html+='<th class="sticky-thead">ချေပြီးငွေ</th>';
                    html+='<th class="sticky-thead">ကျန်ငွေ</th>';
                    html+='<th class="sticky-thead">ငွေချေရက်</th>';
                    html+='</tr>';
                    html+='</thead>';
                    html+='<tbody>';
                    var sList = <?= json_encode($saleList) ?>;
                    var len = sList.length;
                    var len2 = 0;
                    var sum_cmoney=0;
                    var sum_apayment=0;
                    var sum_rbalance=0;
                    var sum_payamt=0;
                    for(var i=0;i<len;i++){
                        if(sList[i].date.substr(start,n)==d_m_y){
                        len2 = len2+1;
                        sum_cmoney= +sum_cmoney + +sList[i].c_money;
                        sum_apayment= +sum_apayment + +sList[i].a_payment;
                        sum_rbalance= +sum_rbalance + +sList[i].r_balance;
                        if(String(sList[i].pay_amt)=='null'){
                            sum_payamt = +sum_payamt + 0;
                        }
                        else{
                            sum_payamt = +sum_payamt + +sList[i].pay_amt;
                        }

                        html+='<tr>';
                        html+='<td>'+len2+'</td>';
                        html+='<td>'+sList[i].date+'</td>';
                        html+='<td>'+sList[i].phone+'</td>';
                        html+='<td>'+sList[i].receipt_no+'</td>';
                        html+='<td>'+sList[i].c_money+'</td>';
                        html+='<td>'+sList[i].a_payment+'</td>';
                        html+='<td>'+String(sList[i].pay_amt).replace('null','')+'</td>';
                        html+='<td>'+sList[i].r_balance+'</td>';
                        html+='<td>'+String(sList[i].pay_date).replace('null','')+'</td>';
                        html+="</tr>";
                        }
                    }
                    html+='</tbody>';
                    if(len2>0){

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td>စုစုပေါင်း</td>';
                    html+='<td>'+sum_cmoney+'</td>';
                    html+='<td>'+sum_apayment+'</td>';
                    html+='<td>'+sum_payamt+'</td>';
                    html+='<td>'+sum_rbalance+'</td>';
                    html+='<td></td>';
                    html+='</tr>';
                    html+='</tfoot>';

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th>စုစုပေါင်း</th>';
                    html+='<th>သင့်ငွေ</th>';
                    html+='<th>စရံ</th>';
                    html+='<th>ချေပြီးငွေ</th>';
                    html+='<th>ကျန်ငွေ</th>';
                    html+='<th></th>';
                    html+='</tr>';
                    html+='</tfoot>';
                    }
                    html+='</table>';
                    $("#result_table").html(html);
                    $("#daily_table").DataTable({
                       "bInfo"           : false,
                       "bLengthChange"   : false,
                       "aaSorting"       : [],
                       "bSort"           : false,
                       "bAutoWidth"      : false,
                        "bFilter"        : false,
                        "bPaginate"      : false,
                        dom: 'Bfrtip',
                        buttons: [
                          { extend: 'print',
                          footer: true,
                          title:'MAW - Photo Studio ('+title+')',
                              customize: function ( win ) {
                                    $(win.document.body)
                                    .css( 'font-size', '14pt' );
                                    // .prepend(

                                    // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                                    // );
                                    $(win.document.body).find( 'table' )
                                    .addClass( 'compact' )
                                    .css( {'font-size': 'inherit','margin-top' : '50px'});
                                    }
                          }
                        ]
                      //"aLengthMenu"   : [5,10,15,20],
                      // "iDisplayLength": 5
                      });
                    var table = $("#daily_table").DataTable();
                    var count = table.rows().count();
                    $("#receipt_count").html(count);
                    $("#result_table").show();
                    $("#bouncher").show();
                }

                else{

                var html='';
                    html+='<table id="daily_table" class="display" cellspacing="0" width="100%">';
                    html+='<thead>';
                    html+='<tr>';
                    html+='<th class="sticky-thead">စဉ်</th>';
                    html+='<th class="sticky-thead">နေ့စွဲ</th>';
                    html+='<th class="sticky-thead">ဖုန်းနံပါတ်</th>';
                    html+='<th class="sticky-thead">ဘောင်ချာ</th>';
                    html+='<th class="sticky-thead">သင့်ငွေ</th>';
                    html+='<th class="sticky-thead">စရံ</th>';
                    html+='<th class="sticky-thead">ချေပြီးငွေ</th>';
                    html+='<th class="sticky-thead">ကျန်ငွေ</th>';
                    html+='<th class="sticky-thead">ငွေချေရက်</th>';
                    html+='</tr>';
                    html+='</thead>';
                    html+='<tbody>';
                    var sList = <?= json_encode($saleList) ?>;
                    var len = sList.length;
                    var len2=0;
                    var sum_cmoney=0;
                    var sum_apayment=0;
                    var sum_rbalance=0;
                    var sum_payamt=0;
                    for(var i=0;i<len;i++){
                        if(sList[i].date.substr(start,n)==d_m_y && sList[i].type==category){
                        len2=len2+1;
                        sum_cmoney = +sum_cmoney + +sList[i].c_money;
                        sum_apayment= +sum_apayment + +sList[i].a_payment;
                        sum_rbalance= +sum_rbalance + +sList[i].r_balance;
                        if(String(sList[i].pay_amt)=='null'){
                            sum_payamt = +sum_payamt + 0;
                        }
                        else{
                            sum_payamt = +sum_payamt + +sList[i].pay_amt;
                        } 
                        html+='<tr>';
                        html+='<td>'+len2+'</td>';
                        html+='<td>'+sList[i].date+'</td>';
                        html+='<td>'+sList[i].phone+'</td>';
                        html+='<td>'+sList[i].receipt_no+'</td>';
                        html+='<td>'+sList[i].c_money+'</td>';
                        html+='<td>'+sList[i].a_payment+'</td>';
                        html+='<td>'+String(sList[i].pay_amt).replace('null','')+'</td>';
                        html+='<td>'+sList[i].r_balance+'</td>';
                        html+='<td>'+String(sList[i].pay_date).replace('null','')+'</td>';
                        html+="</tr>";
                        }
                    }
                    html+='</tbody>';
                    if(len2>0){

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td></td>';
                    html+='<td>စုစုပေါင်း</td>';
                    html+='<td>'+sum_cmoney+'</td>';
                    html+='<td>'+sum_apayment+'</td>';
                    html+='<td>'+sum_payamt+'</td>';
                    html+='<td>'+sum_rbalance+'</td>';
                    html+='<td></td>';
                    html+='</tr>';
                    html+='</tfoot>';

                    html+='<tfoot>';
                    html+='<tr>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th></th>';
                    html+='<th>စုစုပေါင်း</th>';
                    html+='<th>သင့်ငွေ</th>';
                    html+='<th>စရံ</th>';
                    html+='<th>ချေပြီးငွေ</th>';
                    html+='<th>ကျန်ငွေ</th>';
                    html+='<th></th>';
                    html+='</tr>';
                    html+='</tfoot>';

                    }
                    html+='</table>';
                    $("#result_table").html(html);
                    $("#daily_table").DataTable({
                       "bInfo"           : false,
                       "bLengthChange"   : false,
                       "aaSorting"       : [],
                       "bSort"           : false,
                       "bAutoWidth"    : false,
                        "bFilter"      : false,
                        "bPaginate"     : false,
                         dom: 'Bfrtip',
                        buttons: [
                          { extend: 'print',
                          footer: true,
                          title:'Maw - Photo Studio ('+title+')',
                              customize: function ( win ) {
                                    $(win.document.body)
                                    .css( 'font-size', '14pt' );
                                    // .prepend(

                                    // '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                                    // );
                                    $(win.document.body).find( 'table' )
                                    .addClass( 'compact' )
                                    .css( {'font-size': 'inherit','margin-top' : '50px'});
                                    }
                          }
                        ]
                      //"aLengthMenu"   : [5,10,15,20],
                      // "iDisplayLength": 5
                      });
                    var table = $("#daily_table").DataTable();
                    var count = table.rows().count();
                    $("#receipt_count").html(count);
                    $("#result_table").show();
                    $("#bouncher").show();
                   
                }
            "@endif"

            "@if(!isset($saleList))"
               alert("You have no data yet");
            "@endif"
            }
        
        <? if (isset($ASL_Success)) : ?>
        alert("ဒေတာထည့်မှုအောင်မြင်သည်။");
        <? endif; ?>

        <? if(count($errors)): ?>
            alert("ဒေတာထည့်မှုမအောင်မြင်ပါ။");
       <?  endif;   ?>

  });
</script>
</html>