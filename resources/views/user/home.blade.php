
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<!-- <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.dataTables.css') }}">
<link rel="stylesheet" type="text/css" 
href="{{ URL::asset('css/jquery.dataTables_themeroller.css') }}">
<link href="{{ URL::asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/font-awewsome.min.css') }}">
<link rel="icon" type="image/png" href="{{URL::asset('images/logo-icon.png')}}">

<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui.js') }}"></script>
<script src="{{ URL::asset('js/exchange_rate.js') }}"></script>
<script src="{{ URL::asset('js/select2.min.js') }}"></script>
<script src="{{ URL::asset('js/all.js') }}"></script>
<script src="{{ URL::asset('js/font-awesome.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::asset('js/contact.js') }}"></script>
<style>
.container-fluid{
	 height:100vh;
}
.p{
	/*margin-left:5px;
	margin-right:5px;*/
	margin:auto;
	height:180px;
	overflow:hidden;
	border-radius:20px
}
.poi{
	cursor:pointer;
}
.y{
	color:yellow;
}
.cathe_label{

	font-weight:bolder;
	cursor:pointer;
}
/*#BB{
  background: url('../images/BB_test.png');
  background-repeat: no-repeat;
  background-size: 97px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}

#BD{
  background: url('../images/BD_test.png');
  background-repeat: no-repeat;
  background-size: 140px 167px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}

#MJ{
  background: url('../images/MJ_test.png');
  background-repeat: no-repeat;
  background-size: 100px 167px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#DB{
  background: url('../images/DB_test.png');
  background-repeat: no-repeat;
  background-size: 97px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#DD{
  background: url('../images/DD_test.png');
  background-repeat: no-repeat;
  background-size: 97px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#DF{
   background: url('../images/foreign.png');
  background-repeat: no-repeat;
  background-size: 97px 160px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
 -moz-border-radius: 100px;
}
#MC{
  background: url('../images/MC_test.png');
  background-repeat: no-repeat;
  background-size: 110px 180px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}

#MF{
  background: url('../images/license.png');
  background-repeat: no-repeat;
  background-size: 100px 110px;
  background-position: center center;
 -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}
#MS{
  background: url('../images/beauty.png');
  background-repeat: no-repeat;
  background-size: 120px 150px;
  background-position: center center;
  -webkit-border-radius: 100px;
 -moz-border-radius: 100px;
}*/
.w{
	color:white;
}
.contact-link{
	text-decoration:underline;
}
html,body{
	font-family: 'Padauk', sans-serif;
}
</style>
<meta charset="UTF-8">
<title> Maw </title>
<body class="bg-light">
 <div class="container-fluid">
 	<div class="row justify-content-right">
 	   <div class="col-8 pl-5">
 	   	 	<img src="../images/mawlogo.png" width="70" height="70" class="ml-5">
         	<label class="mt-3" style="cursor:default;color:#191970;font-size:20px;">
          		<b>MAW Beauty Salon & Photo Studio</b>
         	</lebel>
       </div>
 		<div class="col-4 mt-3">
 		   <div class="dropdown" style="float:right;">
    			 <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" 
    			 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="fas fa-align-justify"></i>
                 </button>  
    				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
      					
      				    <a class="dropdown-item" href="{{URL::to('user/logout')}}">
      				    <i class="fa fa-door-open"></i>
      				        Log out
      				    </a>
      					<a class="dropdown-item" id="contact">
      					<i class="fa fa-book-open"></i>
      					Contacts
      				    </a>
    				</div>
  			</div>
 		</div>
 	</div>
 	<div id="contactpopup" style="display:none;z-index:999999;overflow-y:hidden;" class="justify-content-center">
			<center> 
  					<h3 style="background-color:#32CD32;color:white"> 
  						ICT for Development Team <br>
  						Global Links Asia Myanmar Co., Ltd. 
  					</h3>
  				<div class="bg-light pb-3">
  					<br>
  					<a href="http://www.ict4development.asia" class="contact-link text-primary">
  						www.ict4development.asia
  				    </a>  
  				    <br>
  					<a href="http://www.glamgroup.biz" class="contact-link text-primary">www.glamgroup.biz</a>
  					<br>
  					<a href="http://www.globallinks21.com"  class="contact-link text-primary">www.globallinks21.com</a>
  					<br><br>
  					<i class="fas fa-envelope"></i> manager@ict4development.asia<br>
  					<i class="fas fa-phone"></i> 09-448015325
  				</div>
			</center>
 	</div>
	<div class="row justify-content-center mt-1">
		<div class="col-3" style="height:580px;border:2px solid blue">
			<div class="row pb-2 pt-2">
				<div class="col border bg-primary p poi btn w" id="BB">
					<img src="../images/BB.jpg" width="70" height="90" class="rounded-circle">
					<label class="cathe_label" >
						<span>အလှူမင်္ဂလာ </span>
						<span style="border-bottom:2px solid lightgrey"><br>အလှမိတ်ကပ်</span>
						<span class="y"> <br> BB </span>
					</label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="BB_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="BB" name="category"> 
					</form>
				</div>

				<div class="col border bg-primary p poi btn w" id="BD">
					<img src="../images/BD.jpg" width="90" height="90" class="rounded-circle">
					<label class="cathe_label">
						<span style="border-bottom:2px solid lightgrey"><br>ဘွဲ့မိတ်ကပ်</span>
						<span class="y"> <br> BD </span>
					</label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="BD_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="BD" name="category"> 
					</form>
				</div>

				<div class="col border bg-primary p poi btn w"  id="MJ">
					<img src="../images/j.jpg" width="70" height="90" class="rounded-circle">
					<label class="cathe_label">
						<span style="border-bottom:2px solid lightgrey"><br>ရတနာ</span>
						<span class="y"> <br> MJ </span>
					</label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="MJ_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="MJ" name="category"> 
					</form>
				</div>
			</div>
			<div class="row pb-2">
				<div class="col border bg-primary p poi btn w" id="DB">
					<img src="../images/DB.jpg" width="70" height="90" class="rounded-circle">
					<label class="cathe_label">
						<span>
							အလှူမင်္ဂလာ<br>
					    </span>
					    <span style="border-bottom:2px solid lightgrey">
						ဝတ်စုံ
					    </span>
						<span class="y"> <br> DB </span>
					</label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="DB_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="DB" name="category"> 
					</form>
				</div>
				<div class="col border bg-primary p poi btn w" id="DD">
					<img src="../images/DD.jpg" width="70" height="90" class="rounded-circle">
					<label class="cathe_label">
					<span>
						ဘွဲ့ဝတ်စုံ
					</span>
					<span style="border-bottom:2px solid lightgrey">
				        ပြည်တွင်း
					</span>
						<span class="y"> <br> DD </span>
					</label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="DD_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="DD" name="category"> 
					</form>
				</div>
				<div class="col border bg-primary p poi btn w"  id="DF">
					<img src="../images/fconvo.jpg" width="70" height="90" class="rounded-circle">
					<label class="cathe_label">
					<span>
						နိုင်ငံခြားဘွဲ့
					</span>
					<span style="border-bottom:2px solid lightgrey">
						ဝတ်စုံ
					</span>
						<span class="y"> <br> DF </span>
					</label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="DF_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="DF" name="category"> 
					</form>
				</div>
			</div>
			<div class="row pb-2">
			<div class="col border bg-primary p poi btn w" id="MC">
				<img src="../images/MC.png" width="70" height="90" class="rounded-circle">
					<label class="cathe_label">
					 <span>
					  &nbsp;ကား <br>
					 </span>
					 <span style="border-bottom:2px solid lightgrey">
                       လက်မှတ်
                     </span>
                       <span class="y"> <br>MC</span>
                   </label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="MC_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="MC" name="category"> 
					</form>
			</div>
			<div class="col border bg-primary p poi btn w" id="MF">
				<img src="../images/f.jpg" width="70" height="90" class="rounded-circle">
			        <label class="cathe_label">
			          <span>
                       ပုံ၊ဘောင်၊
                      </span>
                      <span style="border-bottom:2px solid lightgrey">
                      	လိုင်စင်
                      </span>
                       <span class="y"> <br>MF</span>
                    </label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="MF_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="MF" name="category"> 
					</form>
			</div>
			<div class="col border bg-primary p poi btn w" id="MS">
				    <img src="../images/beauty2.jpg" width="70" height="90" class="rounded-circle">
					  <label class="cathe_label">
					  <span>
					  Beauty<br>
					  </span>
					  <span style="border-bottom:2px solid lightgrey">
                       &nbsp;Salon
                      </span>
                      <span class="y"> <br>MS</span>
                    </label>
					<form method="post" action="{{URL::to('user/ASL')}}" id="MS_Form">
						{{ csrf_field()  }}
						<input type="hidden" value="MS" name="category"> 
					</form>
			</div>
		  </div>
		</div>
		<div class="col-9" style="height:580px;border:2px solid blue;border-left:none;">
			<ul class="nav nav-tabs nav-justified">
    			<li class="nav-item">
      				<a class="nav-link bg-primary alert_nav" style="color:white" href="#">
      				  အရောင်းစာရင်းအပ်ရန် 
      				</a>
    			</li>
    			<li class="nav-item">
      				<a class="nav-link bg-primary alert_nav" style="color:white" href="#">ငွေပေး၊ငွေချေရန်</a>
    			</li>
    			<li class="nav-item">
      				<a class="nav-link bg-primary alert_nav" style="color:white" href="#">စာရင်းဖျက်ရန်</a>
    			</li>
    			<li class="nav-item">
      				<a class="nav-link bg-primary alert_nav" style="color:white" href="#">စာရင်းချုပ်ကြည့်ရန်</a>
    			</li>
  			</ul>

           <div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
            <ul class="carousel-indicators">
    			<li data-target="#demo" data-slide-to="0" class="active"></li>
    			<li data-target="#demo" data-slide-to="1"></li>
    			<li data-target="#demo" data-slide-to="2"></li>
  			</ul>

  <!-- The slideshow -->
  				<div class="carousel-inner">
  				 <div class="carousel-item active">
      				<img src="../images/Maw2.jpg"  style="height:100%;width:100%">
    			  </div>
    			  <div class="carousel-item">
      				<img src="../images/BD_new_new.jpg"  style="height:100%;width:100%">
    			  </div>
    			   <div class="carousel-item">
      				<img src="../images/maw_3.jpg"  style="height:100%;width:100%">
    			  </div>
  				</div>

  <!-- Left and right controls -->
  				<a class="carousel-control-prev" href="#demo" data-slide="prev">
   					 <span class="carousel-control-prev-icon"></span>
  				</a>
  				<a class="carousel-control-next" href="#demo" data-slide="next">
    				<span class="carousel-control-next-icon"></span>
  				</a>
			</div>
		</div>
    </div>
 </div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		$("#BB").click(function(){
			$("#BB_Form").submit();
		});

		$("#BD").click(function(){
			$("#BD_Form").submit();
		});

		$("#MJ").click(function(){
			$("#MJ_Form").submit();
		});

		$("#DB").click(function(){
			$("#DB_Form").submit();
		});

		$("#DD").click(function(){
			$("#DD_Form").submit();
		});

        $("#DF").click(function(){
			$("#DF_Form").submit();
		});

		$("#MC").click(function(){
			$("#MC_Form").submit();
		});

		$("#MF").click(function(){
			$("#MF_Form").submit();
		});
         
        $("#MS").click(function(){
			$("#MS_Form").submit();
		});

		$(".alert_nav").click(function(){
			alert("Please choose button of left menu ! ");
		})

		// $(".w label").css("background-color","rgba(0,0,0,0.1)");

		// $(".w").mouseenter(function(){
		// 	$(this).css("color","white");
		// 	$('label',this).css("background-color","rgba(0,0,0,0.2)");
		// }).mouseleave(function(){
		// 	$('label',this).css("background-color","rgba(0,0,0,0.1)");

		// });



	});
</script>
</html>